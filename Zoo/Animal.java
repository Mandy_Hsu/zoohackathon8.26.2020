package Zoo;

public abstract class Animal {
    private Gender gender;
    private Diet diet;
    private Food[] food;
    private double temperature;
    private Litter litter;

    public Animal(Gender gender, Diet diet, Food[] food, double temperature, Litter litter) {
        this.gender = gender;
        this.diet = diet;
        this.food = food;
        this.temperature = temperature;
        this.litter = litter;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Diet getDiet() {
        return diet;
    }

    public void setDiet(Diet diet) {
        this.diet = diet;
    }

    public Food[] getFood() {
        return food;
    }

    public void setFood(Food[] food) {
        this.food = food;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public Litter getLitter() {
        return litter;
    }

    public void setLitter(Litter litter) {
        this.litter = litter;
    }
    
    
    
}
