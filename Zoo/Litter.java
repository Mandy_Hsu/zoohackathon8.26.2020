package Zoo;

public class Litter {
    private int max;
    
    public Litter(int max){
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int givingBirth(){
        return (int)(Math.random()*max);
    }
    
}